set nocompatible  " No compatible with original VI
filetype off

source ~/.vim/settings.vim
source ~/.vim/plugins.vim
source ~/.vim/colors.vim
